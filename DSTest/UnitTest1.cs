﻿using System.Collections.Generic;
using CounterSoft.Gemini.Commons.Entity;
using DS.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using DS.Services;
using DS.ViewModels;

namespace DSTest
{
    [TestFixture]
    public class UnitTest1
    {
        private IIssueService _testIssueService;
        private MainViewModel _mwm;

        [SetUp]
        public void Setup()
        {           
            _testIssueService = new TestIssueService();
            _mwm = new MainViewModel(_testIssueService);
        }

        [TestMethod]
        public void TestMethod1()
        {
        }
    }

    public class TestIssueService : IIssueService
    {
        public CustomFieldEN[] GetCustomFields()
        {
            throw new System.NotImplementedException();
        }

        public List<string> GetMonths()
        {
            throw new System.NotImplementedException();
        }

        public List<string> GetTeams()
        {
            throw new System.NotImplementedException();
        }

        public List<string> GetSprints()
        {
            throw new System.NotImplementedException();
        }

        public List<ProjectDto> GetProjects()
        {
            throw new System.NotImplementedException();
        }

        public List<IssueDto> GetSeveralProjectIssues(List<string> projectList, string deliveryTeam, string sprint, string productionMonth,
            bool toggleDone)
        {
            throw new System.NotImplementedException();
        }

        public List<IssueDto> GetIssues(string projectName, string deliveryTeam, string sprint, string productionMonth, bool toggleDone)
        {
            throw new System.NotImplementedException();
        }

        public List<SubIssueDto> GetSubIssuesFromIssue(int Id)
        {
            throw new System.NotImplementedException();
        }

        public IssueEN ChangeStatus(int id, int direction)
        {
            throw new System.NotImplementedException();
        }
    }
}
