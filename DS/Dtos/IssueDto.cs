﻿using System.Collections.Generic;
using DS.ViewModels;

namespace DS.Models
{
    public enum Status
    {
        NotStarted,
        InProgress,
        Closed,
        Nothing
    }
    public class IssueDto
    {
        public string Title { get; set; }
        public string Status { get; set; }
        public int Id { get; set; }
        public int EstimatedHours { get; set; }
        public int SubIssuesEstimate { get; set; }
        public string IssueType { get; set; }
        public int LoggedHours { get; set; }
        public string ProjectName { get; set; }
        public List<TimeEntryDto> TimeEntries { get; set; }
        public List<SubIssueDto> subIssues { get; set; }
        public List<CommentDto> CommentList { get; set; }
    }
}
