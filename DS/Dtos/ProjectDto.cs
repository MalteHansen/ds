﻿namespace DS.Models
{
    public class ProjectDto 
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
        public int EstimatedHours { get; set; }
        public int HoursLogged { get; set; }
    }
}
