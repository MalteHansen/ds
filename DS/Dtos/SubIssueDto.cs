﻿using System.Collections.Generic;
using DS.ViewModels;
using Prism.Mvvm;

namespace DS.Models
{
    public class SubIssueDto: BindableBase
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int EstimatedHours { get; set; }
        public int EstimatedMinutes { get; set; }
        public string Status { get; set; }
        public int Id { get; set; }
        public int HoursLogged { get; set; }
        public int MinutesLogged { get; set; }
        public string IssueType { get; set; }
        public string Resources { get; set; }
        public string Priority { get; set; }
        public List<CommentDto> Comment { get; set; }
        public List<TimeEntryDto> TimeEntries { get; set; }
    }
}
