﻿using System;

namespace DS.Models
{
    public class TimeEntryDto
    {
        public string Resource { get; set; }
        public string Comment { get; set; }
        public DateTime EntryDate { get; set; }
        public int Hours { get; set; }
        public int Minutes { get; set; }
    }
}
