﻿namespace DS.ViewModels
{
    public class CommentDto
    {
        public string Username { get; set; }
        public string Comment { get; set; }
    }
}