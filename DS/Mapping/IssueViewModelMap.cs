﻿using System.Linq;
using DS.MapModels;
using DS.Models;

namespace DS.Mapping
{
    public static class IssueViewModelMap
    {
        public static IssueViewModel Map(IssueDto dto)
        {
            return new IssueViewModel(
                dto.Title,
                dto.Id,
                dto.Status,
                dto.EstimatedHours,
                dto.LoggedHours,
                dto.SubIssuesEstimate,
                dto.CommentList.Select(commentDto=> new CommentViewModel(
                    commentDto.Username,
                    commentDto.Comment)).ToList(),
                dto.subIssues.Select(subIssueDto => new SubIssueViewModel(
                    subIssueDto.Title,
                    subIssueDto.Description,
                    subIssueDto.EstimatedHours,
                    subIssueDto.HoursLogged,
                    subIssueDto.MinutesLogged,
                    subIssueDto.Status,
                    subIssueDto.IssueType,
                    subIssueDto.Resources,
                    subIssueDto.Priority,
                    subIssueDto.Comment.Select(commentDto => new CommentViewModel(
                        commentDto.Username,
                        commentDto.Comment)).ToList())).ToList());
        }
    }
}


