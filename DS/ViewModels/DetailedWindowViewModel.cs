﻿using System.Collections.Generic;
using DS.MapModels;
using DS.Models;
using Prism.Mvvm;

namespace DS.ViewModels
{
    public class DetailedWindowViewModel : BindableBase
    {
        private string _title;
        private string _type;
        private Status _status;
        private string _resources;
        private string _priority;
        private List<CommentViewModel> _comment;

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }
        public string IssueType
        {
            get { return _type; }
            set { SetProperty(ref _type, value); }
        }

        public Status Status
        {
            get { return _status; }
            set { SetProperty(ref _status, value); }
        }

        public string Resources
        {
            get { return _resources; }
            set { SetProperty(ref _resources, value); }
        }

        public string Priority
        {
            get { return _priority; }
            set { SetProperty(ref _priority, value); }
        }

        public List<CommentViewModel> CommentList
        {
            get { return _comment; }
            set { SetProperty(ref _comment, value); }
        }

        public DetailedWindowViewModel(SubIssueViewModel subIssue)
        {
            _title = subIssue.Title;
            _type = subIssue.IssueType;
            _status = subIssue.Status;
            _resources = subIssue.Resource;
            _priority = subIssue.Priority;
            _comment = subIssue.CommentList;
        }

        public DetailedWindowViewModel(IssueViewModel subIssue)
        {
            _title = subIssue.Title;
            _status = subIssue.StatusEnum;
            _comment = subIssue.CommentList;
        }
    }
}
