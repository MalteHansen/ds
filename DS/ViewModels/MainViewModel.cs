﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using DS.MapModels;
using DS.Mapping;
using DS.Models;
using DS.Services;
using DS.Views;
using Prism.Commands;
using Prism.Mvvm;

namespace DS.ViewModels
{
    public class MainViewModel : BindableBase
    {
        private readonly IIssueService _iService;

        private ObservableCollection<SelectableItem> _projects;
        private List<IssueDto> _issueDtoList;
        private ObservableCollection<IssueViewModel> _issueList;

        private ObservableCollection<IssueViewModel> _notStartedList;
        private ObservableCollection<IssueViewModel> _ongoingList;
        private ObservableCollection<IssueViewModel> _doneList;

        private List<string> _teamList;
        private List<string> _sprintList;
        private List<string> _productionMonthList;

        private string _team;
        private string _sprint;
        private string _month;

        private bool _isSelected;

        private IssueViewModel _issueSelected;
        private SubIssueViewModel _subIssueSelected;

        public MainViewModel(IIssueService issueService)
        {
            _iService = issueService ?? throw new ArgumentNullException(nameof(issueService));

            var projects = _iService.GetProjects().Select(x => new SelectableItem(x.Title, false));
            
            _projects = new ObservableCollection<SelectableItem>(projects);
            _issueList = new ObservableCollection<IssueViewModel>();
            _issueDtoList = new List<IssueDto>();

            _teamList = _iService.GetTeams();
            _sprintList = _iService.GetSprints();
            _productionMonthList = _iService.GetMonths();

            SearchCommand = new DelegateCommand(() => SearchLists(SelectedTeam, SelectedSprint, SelectedMonth, isSelected));
            SprintCommand = new DelegateCommand(() => GetSprintIssues(SelectedSprint, isSelected));
            QaCommand = new DelegateCommand(() => GetQaIssues(SelectedMonth, isSelected));
            UatCommand = new DelegateCommand(() => GetUatIssues(SelectedMonth, isSelected));
            SupportCommand = new DelegateCommand(() => GetSupportIssues(SelectedMonth, isSelected));

            BurndownCommand = new DelegateCommand(() => BurndownWindow(SelectedSprint, SelectedMonth));
            SubIssueCommand = new DelegateCommand(() => SubIssueWindow(_subIssueSelected));
            IssueCommand = new DelegateCommand(() => IssueWindow(_issueSelected));

            ForwardCommand = new DelegateCommand(() => SwitchStatus(_issueSelected, "Forward"));
            BackwardCommand = new DelegateCommand(() => SwitchStatus(_issueSelected, "Backward"));

        }

        //Selected Properties
        public IssueViewModel SelectedIssueItem
        {
            get { return _issueSelected; }
            set
            {
                if (_issueSelected != value)
                {
                    _issueSelected = value;
                    OnPropertyChanged();
                }
            }
        }
        public SubIssueViewModel SelectedSubIssueItem
        {
            get { return _subIssueSelected; }
            set
            {
                if (_subIssueSelected != value)
                {
                    _subIssueSelected = value;
                    OnPropertyChanged();
                }
            }
        }

        public string SelectedTeam
        {
            get { return _team; }
            set { SetProperty(ref _team, value); }
        }

        public string SelectedSprint
        {
            get { return _sprint; }
            set { SetProperty(ref _sprint, value); }
        }

        public string SelectedMonth
        {
            get { return _month; }
            set { SetProperty(ref _month, value); }
        }

        public bool isSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected == value) return;
                _isSelected = value;
            }
        }


        //List and combobox Properties
        public ObservableCollection<SelectableItem> ProjectList
        {
            get { return _projects; }
            set { SetProperty(ref _projects, value); }
        }
        public ObservableCollection<IssueViewModel> IssueListNotStarted
        {
            get { return _notStartedList; }
            set { SetProperty(ref _notStartedList, value); }
        }

        public ObservableCollection<IssueViewModel> IssueListOnGoing
        {
            get { return _ongoingList; }
            set { SetProperty(ref _ongoingList, value); }
        }

        public ObservableCollection<IssueViewModel> IssueListDone
        {
            get { return _doneList; }
            set { SetProperty(ref _doneList, value); }
        }

        public List<string> sprintList
        {
            get { return _sprintList; }
            set { SetProperty(ref _sprintList, value); }
        }

        public List<string> teamList
        {
            get { return _teamList; }
            set { SetProperty(ref _teamList, value); }
        }

        public List<string> monthList
        {
            get { return _productionMonthList; }
            set { SetProperty(ref _productionMonthList, value); }
        }


        //Commands
        public ICommand ForwardCommand { get; set; }
        public ICommand BackwardCommand { get; set; }
        public ICommand BurndownCommand { get; set; }
        public ICommand SubIssueCommand { get; set; }
        public ICommand IssueCommand { get; set; }
        public ICommand SearchCommand { get; set; }
        public ICommand SprintCommand { get; set; }
        public ICommand QaCommand { get; set; }
        public ICommand UatCommand { get; set; }
        public ICommand SupportCommand { get; set; }


        //Metode til status
        public void SwitchStatus(IssueViewModel issue, string direction)
        {
            switch (issue.StatusEnum)
            {
                case Status.NotStarted:
                    if (direction == "Forward")
                        _iService.ChangeStatus(issue.Id, 3);
                        issue.StatusEnum = Status.InProgress;
                        RefreshLists();
                    break;
                case Status.InProgress:
                    if (direction == "Forward")
                    {
                        _iService.ChangeStatus(issue.Id, 4);
                        issue.StatusEnum = Status.Closed;
                        RefreshLists();
                    }
                    else
                    {
                        _iService.ChangeStatus(issue.Id, 1);
                        issue.StatusEnum = Status.NotStarted;
                        RefreshLists();
                    }
                    break;
                case Status.Closed:
                    if (direction == "Backward")
                        _iService.ChangeStatus(issue.Id, 3);
                        issue.StatusEnum = Status.InProgress;
                        RefreshLists();
                    break;
                default:
                    Console.WriteLine("ERROR");
                    break;
            }
        }


        //Metoder til nye Windows
        public void SubIssueWindow(SubIssueViewModel subIssue)
        {
            new subIssueWindow(subIssue, null).Show();
        }

        public void IssueWindow(IssueViewModel issue)
        {

            new subIssueWindow(null, issue).Show();
        }
        public void BurndownWindow(string sprint, string month)
        {
            var selectedProjects = _projects.Where(x => x.IsSelected).Select(x => x.Description).ToList();
            var project = selectedProjects[0];
            if((sprint != null && sprint != "<None>") && selectedProjects.Count != 0 && selectedProjects.Count < 2 && (month != null && month != "<None>")) { 
            new Burndown(project, sprint, month).Show();
            }
            else
            {
                MessageBox.Show("Choose one projects, sprint and productionmonth", "ERROR");
            }
        }


        //Metoder til at returnere lister efter status
        public ObservableCollection<IssueViewModel> GetNotStartedIssues(ObservableCollection<IssueViewModel> list)
        {
            return new ObservableCollection<IssueViewModel>(list.Where(x => x.StatusEnum == Status.NotStarted).ToList());
        }

        public ObservableCollection<IssueViewModel> GetOngoingIssues(ObservableCollection<IssueViewModel> list)
        {
            return new ObservableCollection<IssueViewModel>(list.Where(x => x.StatusEnum == Status.InProgress).ToList());
        }

        public ObservableCollection<IssueViewModel> GetDoneIssues(ObservableCollection<IssueViewModel> list)
        {
            return new ObservableCollection<IssueViewModel>(list.Where(x => x.StatusEnum == Status.Closed).ToList());
        }


        //Metoder til knapper
        public void GetUatIssues(string productionMonth, bool isSelected)
        {
            var selectedProjects = _projects.Where(x => x.IsSelected).Select(x => x.Description).ToList();
            if ((productionMonth != null && productionMonth != "<None>")  && selectedProjects.Count != 0)
            {
                _issueList.Clear();
                _issueDtoList.Clear();
                _issueDtoList = _iService.GetSeveralProjectIssues(selectedProjects, null, null, productionMonth, isSelected);
                var uatList = _issueDtoList.Where(x => x.IssueType.Contains("UAT"));
                _issueList = new ObservableCollection<IssueViewModel>(uatList.Select(IssueViewModelMap.Map));
                RefreshLists();
            }
            else
            {
                MessageBox.Show("Choose projects and productionmonth", "ERROR");
            }
        }

        public void GetQaIssues(string productionMonth, bool isSelected)
        {
            var selectedProjects = _projects.Where(x => x.IsSelected).Select(x => x.Description).ToList();
            if ((productionMonth != null && productionMonth != "<None>") && selectedProjects.Count != 0)
            {
                _issueList.Clear();
                _issueDtoList.Clear();
                _issueDtoList = _iService.GetSeveralProjectIssues(selectedProjects, null, null, productionMonth, isSelected);
                var qaList = _issueDtoList.Where(x => x.IssueType.Contains("QA"));
                _issueList = new ObservableCollection<IssueViewModel>(qaList.Select(IssueViewModelMap.Map));
                RefreshLists();
            }
            else
            {
                MessageBox.Show("Choose projects and productionmonth", "ERROR");
            }
        }

        public void GetSupportIssues(string productionMonth, bool isSelected)
        {
            var selectedProjects = _projects.Where(x => x.IsSelected).Select(x => x.Description).ToList();
            if ((productionMonth != null && productionMonth != "<None>") && selectedProjects.Count != 0)
            {
                _issueList.Clear();
                _issueDtoList.Clear();
                _issueDtoList = _iService.GetSeveralProjectIssues(selectedProjects, null, null, productionMonth, isSelected);
                var supportList = _issueDtoList.Where(x => x.IssueType.Contains("Support"));
                _issueList = new ObservableCollection<IssueViewModel>(supportList.Select(IssueViewModelMap.Map));
                RefreshLists();
            }
            else
            {
                MessageBox.Show("Choose projects and productionmonth", "ERROR");
            }
        }

        public void GetSprintIssues(string sprint, bool isSelected)
        {
            var selectedProjects = _projects.Where(x => x.IsSelected).Select(x => x.Description).ToList();
            var date = DateTime.Now;
            var getDate = date.Year + "-" + date.Month;
            if ((sprint != null && sprint != "<None>") && selectedProjects.Count != 0)
            {
                _issueList.Clear();
                _issueDtoList.Clear();
                _issueDtoList =
                    _iService.GetSeveralProjectIssues(selectedProjects, null, sprint, getDate, isSelected);
                _issueList = new ObservableCollection<IssueViewModel>(_issueDtoList.Select(IssueViewModelMap.Map));
                RefreshLists();
            }
            else
            {
                MessageBox.Show("Choose projects and sprint", "ERROR");
            }
        }

        public void SearchLists(string deliveryTeam, string sprint, string productionMonth, bool isSelected)
        {
            var selectedProjects = _projects.Where(x => x.IsSelected).Select(x => x.Description).ToList();
            _issueList.Clear();
            _issueDtoList.Clear();

            _issueDtoList = _iService.GetSeveralProjectIssues(selectedProjects, deliveryTeam, sprint, productionMonth, isSelected);
            _issueList = new ObservableCollection<IssueViewModel>(_issueDtoList.Select(IssueViewModelMap.Map));

            RefreshLists();
        }

        public void RefreshLists()
        {
            IssueListNotStarted = GetNotStartedIssues(_issueList);
            IssueListOnGoing = GetOngoingIssues(_issueList);
            IssueListDone = GetDoneIssues(_issueList);
        }
    }
}