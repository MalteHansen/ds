﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using CounterSoft.Gemini.Commons.Extensions;
using DS.Annotations;
using DS.Models;
using DS.Services;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace DS.ViewModels
{
    public class BurndownViewModel: INotifyPropertyChanged
    {
        private PlotModel _plotModel;
        

        private IIssueService _issueService;
        private List<Models.IssueDto> _temp;
        private string[] _productionMonth;
        private string _project;
        private DateTime _dateToUse;

        public BurndownViewModel(string project, string sprint, string productionMonth, IIssueService issueService)
        {
            _issueService = issueService ?? throw new ArgumentNullException(nameof(issueService));

            PlotModel = new PlotModel();
            _temp = _issueService.GetIssues(project, null, sprint, productionMonth, true);

            _project = project;
            _productionMonth = productionMonth.Split('-');
            _dateToUse = new DateTime(Int32.Parse(_productionMonth[0]), Int32.Parse(_productionMonth[1]), 1);

            SetUpModel();
            LoadData();
        }

        public PlotModel PlotModel
        {
            get { return _plotModel; }
            set { _plotModel = value; OnPropertyChanged("PlotModel"); }
        }

        private void SetUpModel()
        {
            PlotModel.LegendTitle = "Legend";
            PlotModel.LegendOrientation = LegendOrientation.Horizontal;
            PlotModel.LegendPlacement = LegendPlacement.Inside;
            PlotModel.LegendPosition = LegendPosition.TopRight;
            PlotModel.LegendBackground = OxyColor.FromAColor(200, OxyColors.White);
            PlotModel.LegendBorder = OxyColors.Black;

            PlotModel.Title = "Burndown Chart";

            var startDate = _dateToUse;
            var endDate = _dateToUse.GetLastDayOfMonth();

            PlotModel.Axes.Add(new DateTimeAxis {
                Position = AxisPosition.Bottom,
                Minimum = DateTimeAxis.ToDouble(startDate),
                Maximum = DateTimeAxis.ToDouble(endDate),
                StringFormat = "dd/MM/yyyy" });

            PlotModel.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Left,
                Minimum = 0,
                Maximum = getMaxEstimate(_temp)
            });
        }

        private void LoadData()
        {
            var days = _dateToUse.GetLastDayOfMonth().Day;
            var timeEntries = getTimeEntries(_temp);
            var maxEstimate = getMaxEstimate(_temp);

            var sortedTimeEntries = timeEntries.OrderBy(x => x.EntryDate);
           
            //Burndown
            var lineSerie = new LineSeries
            {
                StrokeThickness = 2,
                MarkerSize = 3,
                CanTrackerInterpolatePoints = false,
                Title = _project,
                Smooth = false
            };

            foreach (var timeEntry in sortedTimeEntries)
            {          
               lineSerie.Points.Add(new DataPoint
                   (DateTimeAxis
                   .ToDouble(timeEntry.EntryDate), maxEstimate));
                maxEstimate -= timeEntry.Hours;
            }

            PlotModel.Series.Add(lineSerie);

            //Ideel burndown
            var count = getMaxEstimate(_temp);
            var burndownrate = count / days;
            var firstDayOfMonth = _dateToUse;
            var lineSerie2 = new LineSeries
            {
                StrokeThickness = 2,
                MarkerSize = 3,
                CanTrackerInterpolatePoints = false,
                Title = "Ideel",
                Smooth = false
            };
            for (int i = 0; i < days + 1; i++)
            {

                lineSerie2.Points.Add(new DataPoint(DateTimeAxis.ToDouble(firstDayOfMonth), count));
                firstDayOfMonth = firstDayOfMonth.AddDays(1);
                count -= burndownrate;

            }
            PlotModel.Series.Add(lineSerie2);
        }

        private List<TimeEntryDto> getTimeEntries(List<IssueDto> IssueList)
        {
            List<TimeEntryDto> result = new List<TimeEntryDto>();

            foreach (var issue in IssueList)
            {
                foreach (var timeEntry in issue.TimeEntries)
                {
                    result.Add(timeEntry);
                }
                foreach (var subIssue in issue.subIssues)
                {
                    foreach (var timeEntry in subIssue.TimeEntries)
                    {
                        result.Add(timeEntry);
                    }
                }
            }
            return result;
        }

        private int getMaxEstimate(List<IssueDto> IssueList)
        {
            int result = 0;
            foreach (var Issue in IssueList)
            {
                result += Issue.EstimatedHours;
            }
            return result;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
