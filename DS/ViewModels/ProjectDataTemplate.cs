﻿using System.Windows;
using System.Windows.Controls;
using DS.MapModels;

namespace DS.ViewModels
{
    public class ProjectDataTemplate : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            Window window = Application.Current.MainWindow;

            return (DataTemplate) window.FindResource("ProjectTemplate");
        }
    }

    public class IssueDataTemplate : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            Window window = Application.Current.MainWindow;
            IssueViewModel issue = (IssueViewModel) item;

            var count = 0;
            if (issue.SubIssueList != null)
            {
                foreach (var subIssue in issue.SubIssueList)
                {
                    count += subIssue.EstimatedHours;
                }
            }

            if (issue.SubIssueList.Count == 0)
            {
                return (DataTemplate) window.FindResource("NoSubIssueTemplate");
            }

            if (issue.EstimatedHours > count)
            {
                return (DataTemplate) window.FindResource("SuccessTemplate");
            }
                
            return (DataTemplate) window.FindResource("FailureTemplate");         
        }
    }

    public class SubIssueDataTemplate : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            Window window = Application.Current.MainWindow;
            
            return (DataTemplate) window.FindResource("SubIssueTemplate");
        }
    }
}
