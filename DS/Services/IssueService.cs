﻿using CounterSoft.Gemini.Commons.Entity;
using CounterSoft.Gemini.WebServices;
using DS.Models;
using DS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DS.Services
{
    public class IssueService : IIssueService
    {

        private readonly ServiceManager _serviceManager;
        private IAppConfigService _appConfigService = new AppConfigService();

        public IssueService()
        {
            _serviceManager = new ServiceManager(@"https://gemini.d60.dk", _appConfigService.GetUsername(), _appConfigService.GetPassword(), String.Empty, false); 
        }

        //GET metoder til CustomFields
        public CustomFieldEN[] GetCustomFields()
        {
            return _serviceManager.CustomFieldsService.GetCustomFields();  
        }
        public List<string> GetMonths()
        {
            var costumfields = GetCustomFields();
            var productionMonthField = costumfields.Single(x => x.CustomFieldName == "Production Month");
            var tempResult = productionMonthField.LookupData.ToList();
            List<string> result = new List<string>();

            foreach (var entity in tempResult)
            {
                result.Add(entity.GenericValue);
            }

            return result;
        }

        public List<string> GetTeams()
        {
            var costumfields = GetCustomFields();
            var teams = costumfields.Single(x => x.CustomFieldName == "Delivery Team");
            var tempResult = teams.LookupData.ToList();
            List<string> result = new List<string>();

            foreach (var entity in tempResult)
            {
                result.Add(entity.GenericValue);
            }

            return result;
        }

        public List<string> GetSprints()
        {
            var costumfields = GetCustomFields();
            var sprints = costumfields.Single(x => x.CustomFieldName == "Sprint");
            var tempResult = sprints.LookupData.ToList();
            List<string> result = new List<string>();

            foreach (var entity in tempResult)
            {
                result.Add(entity.GenericValue);
            }
            return result;
        }

        //GET metoder til Projects, Issues og SubIssues
        public List<ProjectDto> GetProjects()
        {
            var projects = _serviceManager.ProjectsService.GetProjects();
            var result = new List<ProjectDto>();

            foreach (var project in projects)
            {
                result.Add(new ProjectDto()
                {
                    Title = project.ProjectName,
                    Description = project.ProjectDesc,
                    EstimatedHours = project.ComponentCount,
                    HoursLogged = project.ComponentCount,
                    Id = project.ProjectID
                });
            }
            return result;
        }

        public List<IssueDto> GetSeveralProjectIssues(List<string> projectList, string deliveryTeam, string sprint, string productionMonth, bool toggleDone)
        {
            var result = new List<IssueDto>();

            foreach (var project in projectList)
            {
                var tempList = GetIssues(project, deliveryTeam, sprint, productionMonth, toggleDone);
                foreach (var issue in tempList)
                {
                    result.Add(issue);
                }
            }
            return result;
        }

        public List<IssueDto> GetIssues(string projectName, string deliveryTeam, string sprint, string productionMonth, bool toggleDone)
        {
            var result = new List<IssueDto>();

            var projectId = _serviceManager.ProjectsService.GetProjects().Single(x => x.ProjectName == projectName).ProjectID;
            var userId = _serviceManager.UsersService.WhoAmI().UserID;

            var filter = GetFilter(deliveryTeam, sprint, productionMonth);
            filter.UserID = userId;
            filter.ProjectID = projectId.ToString();
            filter.ExcludeClosed = !toggleDone;
            var issueList = _serviceManager.IssuesService.GetFilteredIssues(filter);

            var customFields = _serviceManager.CustomFieldsService.GetCustomFields();
            var estimatedField = customFields.Single(x => x.CustomFieldName == "Initial Estimate Hrs.");

            foreach (var issue in issueList)
            {
                
                var tempSubIssues = GetSubIssuesFromIssue(issue.IssueID);
                    result.Add(new IssueDto()
                    {
                        Title = issue.IssueSummary,
                        Status = issue.IssueStatusDesc,
                        IssueType = issue.IssueTypeDesc,
                        Id = issue.IssueID,
                        EstimatedHours = GetEstimatedHours(issue, estimatedField.CustomFieldID),
                        SubIssuesEstimate = GetSubEstimates(tempSubIssues),
                        LoggedHours = GetLoggedHours(tempSubIssues),
                        ProjectName = issue.ProjectName,
                        subIssues = tempSubIssues,
                        CommentList = GetComments(issue.IssueComments),
                        TimeEntries = GetTimeEntries(issue.IssueTimeEntries)
                    });
            }

            return result;

        }

        public List<SubIssueDto> GetSubIssuesFromIssue(int Id)
        {
            var subIssues = _serviceManager.IssuesService.GetSubIssues(Id);
            var result = new List<SubIssueDto>();

            foreach (var subIssue in subIssues)
            {

                result.Add(new SubIssueDto
                {
                    Title = subIssue.IssueSummary,
                    Description = subIssue.IssueLongDesc,
                    EstimatedHours = subIssue.EstimateHours,
                    EstimatedMinutes = subIssue.EstimateMinutes,
                    Status = subIssue.IssueStatusDesc,
                    HoursLogged = subIssue.HoursLogged,
                    MinutesLogged = subIssue.MinutesLogged,
                    Id = subIssue.IssueID,
                    IssueType = subIssue.IssueTypeDesc,
                    Priority = subIssue.IssuePriorityDesc,
                    Resources = subIssue.IssueResourcesUserName,
                    Comment = GetComments(subIssue.IssueComments),
                    TimeEntries = GetTimeEntries(subIssue.IssueTimeEntries)
                    
                });
            }

            return result;
        }

        //Metode til at skifte status
        public IssueEN ChangeStatus(int id, int direction)
        {
            var issue = _serviceManager.IssuesService.GetIssue(id);
            issue.IssueStatus = direction;

            var updated = _serviceManager.IssuesService.UpdateIssue(issue);
            return updated;
        }

        //Get metoder til attributter på issues/subissues
        private List<TimeEntryDto> GetTimeEntries(IssueTimeEntryEN[] timeEntriesList)
        {
            List<TimeEntryDto> result = new List<TimeEntryDto>();

            foreach (var timeEntry in timeEntriesList)
            {
                result.Add(new TimeEntryDto
                {
                    Resource = timeEntry.UserFullName,
                    Comment = timeEntry.Comment,
                    EntryDate = timeEntry.TimeEntryDate,
                    Hours = timeEntry.Hours,
                    Minutes = timeEntry.Minutes
                });
            }
            return result;
        }
        private List<CommentDto> GetComments(IssueCommentEN[] commentList)
        {
            List<CommentDto> result = new List<CommentDto>();

            foreach (var comment in commentList)
            {
                result.Add(new CommentDto
                {
                    Username = comment.UserName,
                    Comment = comment.Comment.Replace("<div>", "").Replace("<br />", "").Replace("</div>", "").Replace(System.Environment.NewLine, "")
                });
            }
            return result;
        }

        private int GetEstimatedHours(IssueEN tempIssue, int customFieldId)
        {
            var estimatedHours =  tempIssue.GetCustomFieldData(customFieldId);
            if (estimatedHours.NumericData.ToString().Length < 1)
            {
                return 0;
            }
            
             return Int32.Parse(estimatedHours.NumericData.ToString());
        }

        private int GetSubEstimates(List<SubIssueDto> subIssueList)
        {
            var count = 0;
            
            foreach (var subIssue in subIssueList)
            {
                count += subIssue.EstimatedHours;
            }

            return count;
        }

        private int GetLoggedHours(List<SubIssueDto> subIssueList)
        {
            var count = 0;

            foreach (var subIssue in subIssueList)
            {
                count += subIssue.HoursLogged;
            }

            return count;
        }

        //Get metode til filter
        private IssuesFilterEN GetFilter(string deliveryTeam, string sprint, string productionMonth)
        {
            string tempDeliveryTeam = deliveryTeam;
            string tempSprint = sprint;
            string tempProductionMonth = productionMonth;

            if (deliveryTeam == "<None>" || deliveryTeam == null)
            {
                tempDeliveryTeam = "";
            }
            if (sprint == "<None>" || sprint == null)
            {
                tempSprint = "";
            }
            if (productionMonth == "<None>" || productionMonth == null)
            {
                tempProductionMonth = "";
            }

            var customFields = _serviceManager.CustomFieldsService.GetCustomFields();
            var deliveryTeamField = customFields.Single(x => x.CustomFieldName == "Delivery Team");
            var sprintField = customFields.Single(x => x.CustomFieldName == "Sprint");
            var productionMonthField = customFields.Single(x => x.CustomFieldName == "Production Month");

            var filter = new IssuesFilterEN();

            if (tempDeliveryTeam.Length < 1 && tempSprint.Length < 1 && tempProductionMonth.Length < 1)
            {
                filter = new IssuesFilterEN
                {
                    CustomFields = new GenericEN[]
                    {
                    }
                };
            }
            else if (tempDeliveryTeam.Length < 1 && tempSprint.Length < 1)
            {
                filter =  new IssuesFilterEN
                {
                    CustomFields = new GenericEN[]
                    {
                        new GenericEN(productionMonthField.CustomFieldID.ToString(), tempProductionMonth),
                    }
                };
            }else if (tempSprint.Length < 1 && tempProductionMonth.Length < 1)
            {
                filter = new IssuesFilterEN
                {
                    CustomFields = new GenericEN[]
                    {
                        new GenericEN(deliveryTeamField.CustomFieldID.ToString(), tempDeliveryTeam),
                    }
                };
            }else if (tempDeliveryTeam.Length < 1 && tempProductionMonth.Length < 1)
            {
                filter = new IssuesFilterEN
                {
                    CustomFields = new GenericEN[]
                    {
                        new GenericEN(sprintField.CustomFieldID.ToString(), tempSprint),
                    }
                };
            }else if (tempDeliveryTeam.Length < 1)
            {
                filter = new IssuesFilterEN
                {
                    CustomFields = new GenericEN[]
                    {
                        new GenericEN(sprintField.CustomFieldID.ToString(), tempSprint),
                        new GenericEN(productionMonthField.CustomFieldID.ToString(), tempProductionMonth),
                    }
                };
            }
            else if (tempSprint.Length < 1)
            {
                filter = new IssuesFilterEN
                {
                    CustomFields = new GenericEN[]
                    {
                        new GenericEN(deliveryTeamField.CustomFieldID.ToString(), tempDeliveryTeam),
                        new GenericEN(productionMonthField.CustomFieldID.ToString(), tempProductionMonth),
                    }
                };
            }
            else if (tempProductionMonth.Length < 1)
            {
                filter = new IssuesFilterEN
                {
                    CustomFields = new GenericEN[]
                    {
                        new GenericEN(deliveryTeamField.CustomFieldID.ToString(), tempDeliveryTeam),
                        new GenericEN(sprintField.CustomFieldID.ToString(), tempSprint),
                    }
                };
            }else 
            {
                filter = new IssuesFilterEN
                {
                    CustomFields = new GenericEN[]
                    {
                        new GenericEN(deliveryTeamField.CustomFieldID.ToString(), tempDeliveryTeam),
                        new GenericEN(sprintField.CustomFieldID.ToString(), tempSprint),
                        new GenericEN(productionMonthField.CustomFieldID.ToString(), tempProductionMonth),
                    }
                };
            }
            
            return filter;
        }
    }
}
