﻿using System.Collections.Generic;
using CounterSoft.Gemini.Commons.Entity;
using DS.Models;

namespace DS.Services
{
    public interface IIssueService
    {
        CustomFieldEN[] GetCustomFields();
        List<string> GetMonths();
        List<string> GetTeams();
        List<string> GetSprints();

        List<ProjectDto> GetProjects();

        List<IssueDto> GetSeveralProjectIssues(List<string> projectList, string deliveryTeam,
            string sprint, string productionMonth, bool toggleDone);

        List<IssueDto> GetIssues(string projectName, string deliveryTeam, string sprint,
            string productionMonth, bool toggleDone);

        List<SubIssueDto> GetSubIssuesFromIssue(int Id);

        IssueEN ChangeStatus(int id, int direction);

    }
}
