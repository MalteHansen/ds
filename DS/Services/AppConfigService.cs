﻿using System.Configuration;

namespace DS.Services
{
    public class AppConfigService : IAppConfigService
    {
        private string _username = ConfigurationManager.AppSettings.Get("Username");
        private string _password = ConfigurationManager.AppSettings.Get("Password");

        public string GetUsername()
        {
            return _username;
        }

        public string GetPassword()
        {
            return _password;
        }
    }
}
