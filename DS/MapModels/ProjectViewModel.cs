﻿using Prism.Mvvm;

namespace DS.ViewModels
{
    public class SelectableItem : BindableBase
    {
        private string description;
        private bool selected;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public bool IsSelected
        {
            get { return selected; }
            set { selected = value;  }
        }

        public SelectableItem(string description, bool isSelected)
        {
            Description = description;
            IsSelected = isSelected;
        }
    }
}