﻿using System.Collections.Generic;
using DS.Models;
using Prism.Mvvm;

namespace DS.MapModels
{
    public class SubIssueViewModel : BindableBase
    {
        private string _title;
        private string _description;        
        private int _estimatedHours;        
        private int _loggedHours;
        private int _loggedMinutes;
        private Status _status;        
        private string _issueType;        
        private string _resource;        
        private string _priority;       
        private List<CommentViewModel> _commentList;        

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }
        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }
        public int EstimatedHours
        {
            get { return _estimatedHours; }
            set { SetProperty(ref _estimatedHours, value); }
        }
        public int LoggedHours
        {
            get { return _loggedHours; }
            set { SetProperty(ref _loggedHours, value); }
        }

        public int LoggedMinutes
        {
            get { return _loggedMinutes; }
            set { SetProperty(ref _loggedMinutes, value); }
        }
        public Status Status
        {
            get { return _status; }
            set { SetProperty(ref _status, value); }
        }
        public string IssueType
        {
            get { return _issueType; }
            set { SetProperty(ref _issueType, value); }
        }
        public string Resource
        {
            get { return _resource; }
            set { SetProperty(ref _resource, value); }
        }
        public string Priority
        {
            get { return _priority; }
            set { SetProperty(ref _priority, value); }
        }
        public List<CommentViewModel> CommentList
        {
            get { return _commentList; }
            set { SetProperty(ref _commentList, value); }
        }

        public SubIssueViewModel(string title, string description, int estimatedHours, int loggedHours, int loggedMinutes, string status, string issueType,
            string resource, string priority, List<CommentViewModel> commentList)
        {
            Title = title;
            Description = description;
            EstimatedHours = estimatedHours;
            LoggedHours = loggedHours;
            LoggedMinutes = loggedMinutes;
            Status = GetStatus(status);
            IssueType = issueType;
            Resource = resource;
            Priority = priority;
            CommentList = commentList;
        }

        private Status GetStatus(string status)
        {
            switch (status)
            {
                case "Not Started":
                    return Models.Status.NotStarted;
                case "In Progress":
                    return Models.Status.InProgress;
                case "Closed":
                    return Models.Status.Closed;
                default:
                    return Status.Nothing;
            }
        }
    }
}
