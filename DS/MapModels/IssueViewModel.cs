﻿using System.Collections.Generic;
using DS.Models;
using Prism.Mvvm;

namespace DS.MapModels
{
    public class IssueViewModel : BindableBase
    {
        private string _title;
        private int _id;
        private Status _statusEnum;
        private int _estimatedHours;
        private int _loggedHours;
        private int _subIssuesEstimate;
        private List<SubIssueViewModel> _subIssueList;
        private List<CommentViewModel> _commentList;
        
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }
        public int Id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }
        public Status StatusEnum
        {
            get { return _statusEnum; }
            set { SetProperty(ref _statusEnum, value); }
        }
        public int EstimatedHours
        {
            get { return _estimatedHours; }
            set => SetProperty(ref _estimatedHours, value);
        }
        public int LoggedHours
        {
            get { return _loggedHours; }
            set { SetProperty(ref _loggedHours, value); }
        }
        public int SubIssuesEstimate
        {
            get { return _subIssuesEstimate; }
            set { SetProperty(ref _subIssuesEstimate, value); }
        }
        public List<SubIssueViewModel> SubIssueList
        {
            get { return _subIssueList; }
            set { SetProperty(ref _subIssueList, value); }
        }
        public List<CommentViewModel> CommentList
        {
            get { return _commentList; }
            set { SetProperty(ref _commentList, value); }
        }

        public IssueViewModel(string title, int id, string status, int estimatedHours, int loggedHours, int subIssuesEstimate, List<CommentViewModel> commentList, List<SubIssueViewModel> subIssueList)
        {
            Title = title;
            Id = id;
            StatusEnum = GetStatus(status);
            EstimatedHours = estimatedHours;
            LoggedHours = loggedHours;
            SubIssuesEstimate = subIssuesEstimate;
            SubIssueList = subIssueList;
            CommentList = commentList;
        }

        private Status GetStatus(string status)
        {
            switch (status)
            {
                case "Not Started":
                    return Models.Status.NotStarted;        
                case "In Progress":
                    return Models.Status.InProgress;;
                case "Closed":
                    return Models.Status.Closed;
                default:
                    return Status.Nothing;
            }
        }
    }
}
