﻿using Prism.Mvvm;

namespace DS.MapModels
{
    public class CommentViewModel : BindableBase
    {
        private string _username;
        private string _comment;

        public string UserName
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }

        }
        public string Comment
        {
            get { return _comment; }
            set { SetProperty(ref _comment, value); }
        }

        public CommentViewModel(string username, string comment)
        {
            UserName = username;
            Comment = comment;
        }
    }
}
