﻿using System.Windows;
using DS.MapModels;
using DS.ViewModels;

namespace DS.Views
{
    /// <summary>
    /// Interaction logic for subIssueWindow.xaml
    /// </summary>
    public partial class subIssueWindow : Window
    {
        public subIssueWindow(SubIssueViewModel subIssue, IssueViewModel issue)
        {
            InitializeComponent();
            this.DataContext = subIssue == null ? new DetailedWindowViewModel(issue) : new DetailedWindowViewModel(subIssue);
        }
    }
}
