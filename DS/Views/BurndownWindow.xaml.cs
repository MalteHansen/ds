﻿using System.Windows;
using DS.Services;
using DS.ViewModels;

namespace DS.Views
{
    /// <summary>
    /// Interaction logic for Burndown.xaml
    /// </summary>
    public partial class Burndown : Window
    {

        public Burndown(string project, string sprint, string productionMonth)
        {
            DataContext = new BurndownViewModel(project, sprint, productionMonth, new IssueService());

            InitializeComponent();
        }
    }
}
